import models
from django.contrib import admin


class PostAdmin(admin.ModelAdmin):

    search_fields = ["title", "pub_date"]

    list_display = ["title", "text", "pub_date", "draft"]

    list_filter = ["title", "pub_date", "draft"]


class AlbumAdmin(admin.ModelAdmin):
    search_fields = ["title", "public"]
    list_display = ["title", "images", "public"]


class TagAdmin(admin.ModelAdmin):
    list_display = ["tag"]


class ImageAdmin(admin.ModelAdmin):

    list_display = [
        "__unicode__", "name", "user", "rating",
        "size", "tags_", "albums_", "thumbnail", "pub_date"
    ]
    list_filter = ["tags", "albums", "user"]


admin.site.register(models.Post, PostAdmin)
admin.site.register(models.Comment)
admin.site.register(models.Album, AlbumAdmin)
admin.site.register(models.Tag, TagAdmin)
admin.site.register(models.Image, ImageAdmin)
