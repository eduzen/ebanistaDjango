from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Field
from crispy_forms.bootstrap import PrependedText, PrependedAppendedText
from crispy_forms.bootstrap import FormActions


class ContactForm(forms.Form):
    contact_name = forms.CharField(required=True)
    contact_email = forms.EmailField(required=True)
    content = forms.CharField(
        required=True,
        widget=forms.Textarea
    )

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.add_input(Submit('Enviar', 'Enviar', css_class='btn-primary btn-lg'))

    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        self.fields['contact_name'].label = "Tu nombre:"
        self.fields['contact_email'].label = "Tu email:"
        self.fields['content'].label = "Vuestra consulta?"
