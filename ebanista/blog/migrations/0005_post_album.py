# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_image_thumbnail2'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='album',
            field=models.ForeignKey(blank=True, to='blog.Album', null=True),
        ),
    ]
