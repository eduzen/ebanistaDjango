# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_post_album'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='album',
            field=models.OneToOneField(null=True, blank=True, to='blog.Album'),
        ),
    ]
