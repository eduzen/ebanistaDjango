import datetime
import os
from string import join
from PIL import Image as PImage
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from ebanista.settings import MEDIA_ROOT
from django.core.files import File
from os.path import join as pjoin
from tempfile import *


class Album(models.Model):
    title = models.CharField(max_length=60)
    public = models.BooleanField(default=False)

    def images(self):
        lst = [image.image.name for image in self.image_set.all()]
        lst = ["<a href='/media/{}'>{}</a>".format(
            x, x.split('/')[-1]
            ) for x in lst
        ]
        return join(lst, ', ')
    images.allow_tags = True

    def __unicode__(self):
        return self.title


class Tag(models.Model):
    tag = models.CharField(max_length=50)

    def __unicode__(self):
        return self.tag


class Image(models.Model):

    """Image model class."""

    name = models.CharField(max_length=200)
    description = models.CharField(max_length=600)
    image = models.ImageField(upload_to='fotos/%Y/%m/%d', default='')
    tags = models.ManyToManyField(Tag, blank=True)
    albums = models.ManyToManyField(Album, blank=True)
    pub_date = models.DateTimeField('date published')
    rating = models.IntegerField(default=50)
    width = models.IntegerField(blank=True, null=True)
    height = models.IntegerField(blank=True, null=True)
    user = models.ForeignKey(User, null=True, blank=True)
    thumbnail2 = models.ImageField(upload_to="images/", blank=True, null=True)

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

    def __unicode__(self):
        return self.image.name

    def save(self, *args, **kwargs):
        """Save image dimensions."""
        super(Image, self).save(*args, **kwargs)
        im = PImage.open(pjoin(MEDIA_ROOT, self.image.name))
        self.width, self.height = im.size

        # large thumbnail
        fn, ext = os.path.splitext(self.image.name)
        im.thumbnail((128, 128), PImage.ANTIALIAS)
        thumb_fn = fn + "-thumb2" + ext
        tf2 = NamedTemporaryFile()
        im.save(tf2.name, "JPEG")
        self.thumbnail2.save(thumb_fn, File(open(tf2.name)), save=False)
        tf2.close()

        super(Image, self).save(*args, ** kwargs)

    def size(self):
        """Image size."""
        return "%s x %s" % (self.width, self.height)

    def tags_(self):
        lst = [x[1] for x in self.tags.values_list()]
        return str(join(lst, ', '))

    def albums_(self):
        lst = [x[1] for x in self.albums.values_list()]
        return str(join(lst, ', '))

    def thumbnail(self):
        return '<a href="/media/{}"><img border="0" alt="" src="/media/{}"\
            height="40" /></a>'.format(self.image.name, self.image.name)
    thumbnail.allow_tags = True


class Post(models.Model):

    """Post model class."""

    title = models.CharField(max_length=200)
    text = models.TextField()
    pub_date = models.DateTimeField(auto_now_add=True)
    draft = models.BooleanField(default=True)
    album = models.OneToOneField(Album, blank=True, null=True)

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

    def __unicode__(self):
        return self.title


class Comment(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    author = models.CharField(max_length=60)
    body = models.TextField()
    post = models.ForeignKey(Post)

    def __unicode__(self):
        return unicode("{}: {}".format(self.post, self.body[:60]))
