from django.test import TestCase, LiveServerTestCase, Client
from django.utils import timezone
from blog.models import Post


class PostTest(TestCase):
    def test_case_post(self):
        # create the post
        post = Post()

        # Set the attributes

        post.title = 'My first post'

        post.text = 'This is my first blog post'

        post.pub_date = timezone.now()

        post.draft = False

        post.save()

        # Check we can find it

        all_posts = Post.objects.all()

        self.assertEquals(len(all_posts), 1)

        only_post = all_posts[0]

        self.assertEquals(only_post, post)

        # Check attributes

        self.assertEquals(only_post.title, 'My first post')

        self.assertEquals(only_post.text, 'This is my first blog post')

        self.assertEquals(only_post.pub_date.day, post.pub_date.day)

        self.assertEquals(only_post.pub_date.month, post.pub_date.month)

        self.assertEquals(only_post.pub_date.year, post.pub_date.year)

        self.assertEquals(only_post.pub_date.hour, post.pub_date.hour)

        self.assertEquals(only_post.pub_date.minute, post.pub_date.minute)

        self.assertEquals(only_post.pub_date.second, post.pub_date.second)


class AdminTest(LiveServerTestCase):
    def test_login(self):
        c = Client()

        response = c.get('/admin/', follow=True)

        # Check response code

        self.assertEquals(response.status_code, 200)

        # Check 'Log in' in response

        self.assertTrue('Log in' in response.content)

        # Log the user in

        c.login(username='mantra', password="mantra")

        # Check response code

        response = c.get('/admin')

        self.assertEquals(response.status_code, 200)

        # Check 'Log out' in response

        self.assertTrue('Log out' in response.content)
