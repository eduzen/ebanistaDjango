from django.conf.urls import url
from django.views.generic import ListView
from . import models
from . import views


urlpatterns = [
    url(r'^$', views.post_list, name='post_list'),
    # url(r'^$', views.main, name='list'),
    url(r'^contacto$', views.contacto, name="contacto"),
    url(r'^about$', views.about, name="about"),
    url(r'^trabajos$', views.main, name="list"),
    url(r'^post$', ListView.as_view(model=models.Post, )),
    url(r'^post/(?P<pk>[0-9]+)/$', views.post_detail, name='post_detail'),
    url(r"^album/(\d+)/$", views.album, name='album'),
    url(r"^image/(\d+)/$", views.image, name="image"),
    url(r'^index$', views.index, name="index"),
]
