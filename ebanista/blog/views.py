# -*- coding: utf-8 -*-
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.shortcuts import HttpResponse
from django.shortcuts import render, render_to_response, redirect
from django.shortcuts import get_object_or_404
from django.core.mail import EmailMessage
from django.utils import timezone
from models import Album, Image
from models import Post
from ebanista.settings import MEDIA_URL
from blog.forms import ContactForm


def main(request):
    """Main listing."""
    albums = Album.objects.all()

    if not request.user.is_authenticated():
        albums = albums.filter(public=True)

    paginator = Paginator(albums, 10)

    try:
        page = int(request.GET.get("page", '1'))
    except ValueError:
        page = 1

    try:
        albums = paginator.page(page)
    except (InvalidPage, EmptyPage):
        albums = paginator.page(paginator.num_pages)

    for album in albums.object_list:
        album.images = album.image_set.all()[:4]

    return render_to_response(
        "blog/list.html",
        dict(
            albums=albums,
            user=request.user,
            media_url=MEDIA_URL
        )
    )


def post_list(request):
    posts = Post.objects.filter(
        pub_date__lte=timezone.now()
    ).order_by('pub_date')

    return render(request, 'blog/post_list.html', {'posts': posts})


def post_detail(request, pk):

    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/post_detail.html', {'post': post})


def album(request, pk, view="thumbnails"):
    """Album listing."""
    num_images = 30

    if view == "full":
        num_images = 10

    album = Album.objects.get(pk=pk)
    if not album.public and not request.user.is_authenticated():
        return HttpResponse(
            "Error: you need to be logged in to view this album."
        )

    images = album.image_set.all()
    paginator = Paginator(images, 30, num_images)

    try:
        page = int(request.GET.get("page", '1'))
    except ValueError:
        page = 1

    try:
        images = paginator.page(page)
    except (InvalidPage, EmptyPage):
        images = paginator.page(paginator.num_pages)

    return render_to_response(
        "blog/album.html",
        dict(
            album=album,
            images=images,
            view=view,
            user=request.user,
            media_url=MEDIA_URL
        )
    )


def image(request, pk):
    """Image page."""
    img = Image.objects.get(pk=pk)
    return render_to_response(
        "blog/image.html",
        dict(
            image=img,
            user=request.user,
            # backurl=request.META["HTTP_REFERER"],
            media_url=MEDIA_URL
        )
    )


def contacto(request):
    form_class = ContactForm

    if request.method == 'POST':
        form = form_class(data=request.POST)

        if form.is_valid():
            contact_name = request.POST.get(
                'contact_name',
                ''
            )
            contact_email = request.POST.get(
                'contact_email',
                '',
            )
            form_content = request.POST.get(
                'content',
                ''
            )

            data = {
                'contact_name': contact_name,
                'contact_email': contact_email,
                'form_content': form_content,
            }
            content = u"Hola Santiago, {contact_name} escribio en la web"
            u"lo siguiente: <br> {form_content} <br> Si querés"
            u"escribirle su mail es {contact_email}".format(**data)

            email = EmailMessage(
                "Nuevo contacto",
                content,
                "Ebanisteria" + ' Santiago',
                ['santiago@gmail.com'],
                headers={'Reply-To': contact_email}
            )
            email.send()
            return redirect('/contacto')

    return render(
                  request,
                  "blog/contacto.html",
                  {'form': form_class}
                  )


def about(request):
    return render_to_response("blog/about.html")


def index(request):
    return render_to_response("blog/index.html")
